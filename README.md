# Sparq\Cache

[![pipeline status](https://gitlab.com/sparq-php/cache/badges/master/pipeline.svg)](https://gitlab.com/sparq-php/cache/commits/master)
[![Latest Stable Version](https://poser.pugx.org/sparq-php/cache/v/stable)](https://packagist.org/packages/sparq-php/cache)
[![coverage report](https://gitlab.com/sparq-php/cache/badges/master/coverage.svg)](https://gitlab.com/sparq-php/cache/commits/master)
[![Total Downloads](https://poser.pugx.org/sparq-php/cache/downloads)](https://packagist.org/packages/sparq-php/cache)
[![License](https://poser.pugx.org/sparq-php/cache/license)](https://packagist.org/packages/sparq-php/cache)

A simple to use cache class.

### Installation and Autoloading

The recommended method of installing is via [Composer](https://getcomposer.org/).

Run the following command from your project root:

```bash
$ composer require sparq-php/cache
```

### Objectives

* Simple to use cache
* Easy integration with other packages/frameworks
* Fast and low footprint

### Usage

```php
require_once __DIR__ . "/vendor/autoload.php";

use RedisClient\RedisClient;
use Sparq\Cache\AbstractCache;

class Foo extends AbstractCache
{
	public function events()
    {
        return [
            'before.get' => function ($key) {
            	if ('myObject' === $key) {
                    $this->set($key, (object) [
                        'some' => 'value',
                    ]);
                }

                if ('myArray' === $key) {
                    $this->set($key, [
                        'some' => 'value',
                    ]);
                }

                if ('some' === $key) {
                    $this->set($key, 'value');
                }
            }
		];
	}
}

$Foo = new Foo();
$Foo->register('memory', new \Sparq\Cache\Adapter\Memory(), 100);
$Foo->register('redis', new \Sparq\Cache\Adapter\RedisExtension(new Redis()), 50);
$Foo->register('redis', new \Sparq\Cache\Adapter\RedisPhpClient(new RedisClient()), 40);

echo $Foo->myObject->some;
echo $Foo->myArray['some'];
echo $Foo->some;
```