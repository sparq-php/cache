<?php

namespace Sparq\Cache\Test;

use PHPUnit\Framework\TestCase;
use RedisClient\RedisClient;

class FooTestRedisPhpClient extends TestCase
{
    public function testRedisAdapter()
    {
        $RedisClient = new RedisClient([
            'server' => '127.0.0.1:6379',
            'database' => 2,
        ]);

        $Foo = new Foo('prefix');
        $Foo->register('redis', new \Sparq\Cache\Adapter\RedisPhpClient($RedisClient));

        $this->assertEquals($Foo->object->some, 'value');
        $this->assertEquals($Foo->array['some'], 'value');
        $this->assertEquals($Foo->some, 'value');
        $this->assertEquals($Foo->missing, null);
    }
}
