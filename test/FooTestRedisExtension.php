<?php

namespace Sparq\Cache\Test;

use Exception;
use PHPUnit\Framework\TestCase;
use Redis;

class FooTestRedisExtension extends TestCase
{
    public function testRedisAdapter()
    {
        $Redis = new Redis();

        if (!$Redis->connect('localhost')) {
            throw new Exception('Error connecting to Redis');
        }
        $Redis->select(1);

        $Foo = new Foo('prefix');
        $Foo->register('redis', new \Sparq\Cache\Adapter\RedisExtension($Redis));

        $this->assertEquals($Foo->object->some, 'value');
        $this->assertEquals($Foo->array['some'], 'value');
        $this->assertEquals($Foo->some, 'value');
        $this->assertEquals($Foo->missing, null);
    }
}
