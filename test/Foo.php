<?php

namespace Sparq\Cache\Test;

use Sparq\Cache\AbstractCache;

class Foo extends AbstractCache
{
    public function events()
    {
        return [
            'notfound' => function ($key) {
                if ('object' === $key) {
                    return (object) [
                        'some' => 'value',
                    ];
                }

                if ('array' === $key) {
                    return [
                        'some' => 'value',
                    ];
                }

                if ('some' === $key) {
                    return 'value';
                }
            },
        ];
    }
}
