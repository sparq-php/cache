<?php

namespace Sparq\Cache\Test;

use PHPUnit\Framework\TestCase;

class FooTest extends TestCase
{
    public function testMemoryAdapter()
    {
        $Foo = new Foo('prefix');
        $Foo->register('memory', new \Sparq\Cache\Adapter\Memory());

        $this->assertEquals($Foo->object->some, 'value');
        $this->assertEquals($Foo->array['some'], 'value');
        $this->assertEquals($Foo->some, 'value');
        $this->assertEquals($Foo->missing, null);
    }
}
