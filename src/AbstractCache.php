<?php

namespace Sparq\Cache;

use InvalidArgumentException;
use Sparq\Event\EventTrait;

/**
 * Cache.
 */
abstract class AbstractCache
{
    use EventTrait;

    private $adapters;
    private $algorithm;
    private $prefix;

    /**
     * Construct method.
     */
    public function __construct($prefix, $default_algorithm = 'md5')
    {
        /*
         * Validate parameters
         */

        if (0 === mb_strlen($prefix)) {
            throw new InvalidArgumentException('Prefix is missing');
        }

        /*
         * Set default values
         */

        $this->adapters = [];
        $this->algorithm = $default_algorithm;
        $this->prefix = $prefix;

        /*
         * Register events
         */

        foreach ($this->events() as $key => $value) {
            $this->on($key, $value);
        }
    }

    /**
     * Gets value from memcache.
     *
     * @param string $key Property name
     *
     * @return any Value
     */
    public function __get($key)
    {
        return $this->get($key, null);
    }

    /**
     * Get cache value.
     *
     * @param string $cache_key Cache Key
     *
     * @return any Value
     */
    public function get($cache_key)
    {
        $adapter_key = $this->key($cache_key);
        $cache_value = null;

        /*
         * Find cache in adapters
         */

        $missing_in = [];
        foreach ($this->adapters as $key => $adapter) {
            if (null !== $cache_value) {
                continue;
            }

            $value = $adapter['handler']->get($adapter_key);

            if (null !== $value) {
                $cache_value = $value;
            } else {
                $missing_in[] = $key;
            }
        }

        /*
         * Emit notfound if value was not found
         */

        if ($this->hasEvent('notfound') && null === $cache_value) {
            $options = $this->emit('options');

            $cache_value = $this->emit('notfound', [$cache_key, $options]);

            if ($this->hasEvent('sanitize') && null !== $cache_value) {
                $cache_value = $this->emit('sanitize', [$cache_value]);
            }
        }

        /*
         * Populate adapters cache (in order)
         */

        if (count($missing_in) > 0 && null !== $cache_value) {
            foreach ($missing_in as $key) {
                $this->adapters[$key]['handler']->set($adapter_key, $cache_value);
            }
        }

        if ($this->hasEvent('before.get')) {
            $cache_value = $this->emit('before.get', [$cache_value]);
        }

        return $cache_value;
    }

    /**
     * Create a custom key.
     *
     * @param string $key Key
     *
     * @return string Hash Key
     */
    final public function key($key)
    {
        if ($this->hasEvent('before.key')) {
            $key = $this->emit('before.key', [$key]);
        }

        if (is_object($key) && ('MongoId' === get_class($key) || 'MongoDB\BSON\ObjectId' === get_class($key))) {
            $key = (string) $key;
        }

        return $this->prefix.':'.hash($this->algorithm, $key);
    }

    /**
     * Set value for a specific key.
     *
     * @param string              $cache_key Raw cache key
     * @param array|object|scalar $value     Property value
     * @param array               $options   Options
     */
    public function set($cache_key, $value, array $options = [])
    {
        /*
         * Validate value
         */

        if (!is_scalar($value) && !is_object($value) && !is_array($value)) {
            throw new InvalidArgumentException('Value can only be array, object or scalar');
        }

        if ($this->hasEvent('before.set')) {
            $value = $this->emit('before.set', [$value]);
        }

        /*
         * Add value
         */

        $adapter_key = $this->key($cache_key);

        foreach ($this->adapters as $adapter) {
            $adapter['handler']->set($adapter_key, $value);
        }
    }

    /**
     * Clear.
     */
    public function clear()
    {
        foreach ($this->adapters as $adapter) {
            $adapter['handler']->clear();
        }

        return true;
    }

    /**
     * Remove key.
     *
     * @param string $cache_key Cache Key
     */
    public function delete($cache_key)
    {
        $adapter_key = $this->key($cache_key);

        foreach ($this->adapters as $adapter) {
            $adapter['handler']->delete($adapter_key);
        }

        return true;
    }

    /**
     * Checks the internal data to see if value exists.
     *
     * @param string $cache_key Cache Key
     *
     * @return bool
     */
    final public function has($cache_key)
    {
        $adapter_key = $this->key($cache_key);

        $has = false;
        foreach ($this->adapters as $adapter) {
            if (true === $adapter['handler']->has($adapter_key)) {
                return true;
            }
        }

        return $has;
    }

    /**
     * Register Adapter.
     *
     * @param string          $description Adapter description
     * @param AbstractAdapter $Adapter     Adapter Interface
     * @param int             $priority    Priority
     */
    final public function register($description, AbstractAdapter $Adapter, $priority = 0)
    {
        /*
         * Add options
         */

        $Adapter->options([
            'prefix' => $this->prefix,
        ]);

        /*
         * Register adapter
         */

        $this->adapters[] = [
            'description' => $description,
            'handler' => $Adapter,
            'priority' => $priority,
        ];

        /*
         * Prioritize
         */

        usort($this->adapters, function ($a, $b) {
            return ($a['priority'] > $b['priority']) ? -1 : 1;
        });
    }
}
