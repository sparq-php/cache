<?php

namespace Sparq\Cache\Adapter;

use Sparq\Cache\AbstractAdapter;

/**
 * In Memory adapter.
 */
class Memory extends AbstractAdapter
{
    private $collection;
    private $collection_key_order;
    private $max_rows;

    /**
     * Construct.
     *
     * @param int $max_rows Max Rows in collection
     */
    final public function __construct($max_rows = 500)
    {
        $this->collection = [];

        $this->max_rows = $max_rows;
    }

    /**
     * Get key value.
     *
     * @param string $key     Key
     * @param any    $default Default key value
     *
     * @return any Key Value
     */
    final public function get($key, $default = null)
    {
        if (!$this->has($key)) {
            return $default;
        }

        return $this->collection[$key];
    }

    /**
     * Set key value.
     *
     * @param string $key   Key
     * @param any    $value Key value
     * @param int    $ttl   Time to live
     */
    final public function set($key, $value, array $options = [])
    {
        if (count($this->collection) >= $this->max_rows) {
            $remove_key = array_shift($this->collection_key_order);
            unset($this->collection[$remove_key]);
        }

        $this->collection[$key] = $value;
        $this->collection_key_order[] = $key;
    }

    /**
     * Delete key value.
     *
     * @param string $key Key
     */
    final public function delete($key)
    {
        if (!$this->has($key)) {
            return false;
        }

        unset($this->collection[$key]);

        return true;
    }

    /**
     * Clear all keys.
     */
    final public function clear()
    {
        $this->collection = [];

        return true;
    }

    /**
     * Has key value.
     *
     * @param string $key Key
     */
    final public function has($key)
    {
        return (bool) isset($this->collection[$key]);
    }
}
