<?php

namespace Sparq\Cache\Adapter;

use Exception;
use Sparq\Cache\AbstractAdapter;
use RedisClient\RedisClient;

/**
 * RedisPhpClient adapter.
 */
class RedisPhpClient extends AbstractAdapter
{
    private $RedisClient;
    private $ttl;

    /**
     * Construct.
     *
     * @param Redis $Redis Redis connection
     * @param int   $ttl   Time to live (seconds)
     */
    final public function __construct(RedisClient $RedisClient, $ttl = 60)
    {
        $this->RedisClient = $RedisClient;

        $this->ttl = $ttl;
    }

    /**
     * Get key value.
     *
     * @param string $key     Key
     * @param any    $default Default key value
     *
     * @return any Key Value
     */
    final public function get($key, $default = null)
    {
        /*
         * Fetch data
         */

        $raw_data = $this->RedisClient->get($key);

        if (null === $raw_data) {
            return $default;
        }

        /*
         * Decode data
         */

        $data = json_decode($raw_data);

        if (null === $data) {
            throw new Exception('Miss cache for '.$key.' with '.$raw_data.' type '.gettype($raw_data));
        }

        /*
         * Transform value
         */

        if ('array' === $data->metadata->type) {
            return (array) $data->value;
        } elseif ('object' === $data->metadata->type) {
            return (object) $data->value;
        }

        return $data->value;
    }

    /**
     * Set key value.
     *
     * @param string $key     Key
     * @param any    $value   Key value
     * @param array  $options Options
     */
    final public function set($key, $value, array $options = [])
    {
        /*
         * TTL
         */

        $ttl = (isset($options['ttl']) && $options['ttl'] > 0) ? $options['ttl'] : $this->ttl;

        /*
         * Data Type
         */

        $type = 'scalar';
        if (is_array($value)) {
            $type = 'array';
        } elseif (is_object($value)) {
            $type = 'object';
        }

        /*
         * Data
         */

        $data = [
            'metadata' => [
                'type' => $type,
            ],
            'value' => $value,
        ];

        return $this->RedisClient->setEx($key, $ttl, json_encode($data));
    }

    /**
     * Delete key value.
     *
     * @param string $key Key
     */
    final public function delete($key)
    {
        return $this->RedisClient->del($key);
    }

    /**
     * Clear all keys.
     *
     * @param array $options Options
     */
    final public function clear(array $options = [])
    {
        $match = $this->prefix.':*';

        foreach ($this->RedisClient->keys($match) as $redis_key) {
            $this->delete($redis_key);
        }

        return true;
    }

    /**
     * Has key value.
     *
     * @param string $key Key
     */
    final public function has($key)
    {
        $res = $this->RedisClient->exists($key);

        if (is_bool($res)) {
            return $res;
        }

        return (0 === $res) ? false : true;
    }
}
