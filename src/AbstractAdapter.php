<?php

namespace Sparq\Cache;

/*
 * Abstract Adatper
 */
abstract class AbstractAdapter
{
    protected $prefix = '';

    /**
     * Get key value.
     *
     * @param string $key     Key
     * @param any    $default Default key value
     *
     * @return any Key Value
     */
    abstract public function get($key, $default = null);

    /**
     * Set key value.
     *
     * @param string $key     Key
     * @param any    $value   Key value
     * @param array  $options Options
     */
    abstract public function set($key, $value, array $options = []);

    /**
     * Delete key value.
     *
     * @param string $key Key
     */
    abstract public function delete($key);

    /**
     * Clear all keys.
     */
    abstract public function clear();

    /**
     * Has key value.
     *
     * @param string $key Key
     */
    abstract public function has($key);

    /**
     * Set options.
     *
     * @param array $options Options
     */
    final public function options(array $options = [])
    {
        $this->prefix = (isset($options['prefix'])) ? $options['prefix'] : '';
    }
}
